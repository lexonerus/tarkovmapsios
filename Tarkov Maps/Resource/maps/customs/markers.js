// SETUP иконка выхода ЧВК
var exitpmc = L.icon({
    iconUrl: 'images/exit.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ДИКОГО
var exitscav = L.icon({
    iconUrl: 'images/exit-scav.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ЧВК возможно
var exitmb = L.icon({
    iconUrl: 'images/exit-maybe.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});


//Слои для выходов ЧВК
var 	zb11 = new L.Marker([0.00923, 0.0343], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-1011|ZB-1011</div>'
    		})
	}),
	zb12 = new L.Marker([0.00999, 0.02933], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-1012|ZB-1012</div>'
    		})
	}),
	gast = new L.Marker([0.00764, 0.02532], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Старая заправка|Old Gas station</div>'
    		})
	}),
	ob = new L.Marker([0.01992, 0.02188], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги|Hostel exit</div>'
    		})
	}),
	ob1 = new L.Marker([0.01992, 0.02188], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги|Hostel exit</div>'
    		})
	}),
	lod = new L.Marker([0.01722, 0.01536], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка контрабандиста|Smugglers boat</div>'
    		})
	}),
	rf = new L.Marker([0.00881, 0.0156], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост ВС РФ|RF post</div>'
    		})
	}),
	cros = new L.Marker([0.01173, 0.00706], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Перекресток|Crossroads</div>'
    		})
	}),
	tr = new L.Marker([0.00577, 0.00582], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Трейлерный парк|Trailers</div>'
    		})
	});


var locationspmc1 = [
                ["LOCATION_1",0.01182, 0.00809],
                ["LOCATION_2",0.01302, 0.0132],
                ["LOCATION_3",0.00903, 0.00755],
                ["LOCATION_4",0.0065, 0.01015],
                ["LOCATION_5",0.00635, 0.00635],
		["LOCATION_6",0.01017, 0.00966]
                ];

var locationspmc2 = [
                ["LOCATION_1",0.01154, 0.03255],
                ["LOCATION_2",0.00963, 0.0326],
                ["LOCATION_3",0.01032, 0.03095],
                ["LOCATION_4",0.01218, 0.03123],
                ["LOCATION_5",0.01178, 0.02898],
		["LOCATION_6",0.01442, 0.03071],
                ];

var pmc1 = L.layerGroup([zb11, zb12, gast, ob]);
var pmc2 = L.layerGroup([ob1, lod, rf, cros, tr]);


for (var i = 0; i < locationspmc1.length; i++) {
                        circles = new L.circle([locationspmc1[i][1],locationspmc1[i][2]], 150, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")		
			.addTo(pmc1);

};
	
for (var i = 0; i < locationspmc2.length; i++) {
                        circles = new L.circle([locationspmc2[i][1],locationspmc2[i][2]], 200, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")		
			.addTo(pmc2);

};


//Слови для выходов Диких

var 	gasts = new L.Marker([0.00764, 0.02532], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Старая заправка|Old Gas station</div>'
    		})
	}),
	obs = new L.Marker([0.01992, 0.02188], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги|Hostel exit</div>'
    		})
	}),
	ob1s = new L.Marker([0.01992, 0.02188], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги|Hostel exit</div>'
    		})
	}),
	lods = new L.Marker([0.01722, 0.01536], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка контрабандиста|Smugglers boat</div>'
    		})
	}),
	rfs = new L.Marker([0.00881, 0.0156], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост ВС РФ|RF post</div>'
    		})
	}),
	cross = new L.Marker([0.01173, 0.00706], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Перекресток|Crossroads</div>'
    		})
	}),
	trs = new L.Marker([0.00571, 0.00757], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Времянка у трейлерного парка|Trailers</div>'
    		})
	}),
	gd = new L.Marker([0.00608, 0.01086], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД до Таркова|Rails to Tarkov</div>'
    		})
	}),
	gd1 = new L.Marker([0.01549, 0.0126], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД к порту|Rails to Port</div>'
    		})
	}),
	sk = new L.Marker([0.01127, 0.0175], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Склад 17|Warehouse 17</div>'
    		})
	}),
	sn = new L.Marker([0.01781, 0.01695], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост снайперов|Snipers post</div>'
    		})
	}),
	vr = new L.Marker([0.01361, 0.0235], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Заводские времянки|Factory temporaries</div>'
    		})
	}),
	sk1 = new L.Marker([0.01326, 0.02536], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Склад 4|Warehouse 4</div>'
    		})
	}),
	gd2 = new L.Marker([0.01673, 0.02548], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД к военной базе|Rails to military</div>'
    		})
	}),
	cc = new L.Marker([0.01615, 0.02653], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">За цистерной|Above the tank</div>'
    		})
	}),
	vr1 = new L.Marker([0.01547, 0.02984], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Времянка|Temporary</div>'
    		})
	}),
	kpp = new L.Marker([0.0144, 0.03163], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Военный КПП|Military checkpoint</div>'
    		})
	}),
	adm = new L.Marker([0.0122, 0.03419], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Административные ворота|Admin gates</div>'
    		})
	}),
	cor = new L.Marker([0.0083, 0.03489], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дальний угол завода|Far factory corner</div>'
    		})
	});

var locationscavs = [
                ["LOCATION_1",0.01279, 0.02935],
                ["LOCATION_2",0.01066, 0.02865],
                ["LOCATION_3",0.0139, 0.03094],
		["LOCATION_4",0.01501, 0.02653],
		["LOCATION_6",0.01532, 0.02193],
		["LOCATION_7",0.018, 0.02108],
		["LOCATION_8",0.01392, 0.01842],
		["LOCATION_9",0.01051, 0.01556],
		["LOCATION_10",0.01425, 0.01547],
		["LOCATION_11",0.01137, 0.00914],
                ];

var scav = L.layerGroup([gast, obs, ob1s, lods, rfs, cross, trs, gd, 
			 gd1, sk, sn, vr, sk1, gd2, cc, vr1, kpp, adm, cor]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 150, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")		
			.addTo(scav);
};

var clearmap = L.layerGroup([]);


//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});
//Добавление слоев на карту
var overlays = {
	"Чистая карта": clearmap,
	"ЧВК респ слева": pmc1,
	"ЧВК респ справа": pmc2,
	"Дикие" : scav
};
//Создание кнопки управлния слоями
L.control.layers(overlays).addTo(mymap);
