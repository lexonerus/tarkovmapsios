var mymap = L.map('mymap').setView([0.02081, 0.02824], 15);

            L.tileLayer('maps/woods/{z}/{x}/{y}.png', {
            minZoom: 13,
            maxZoom: 17

            }).addTo(mymap);
            
            var popup = L.popup();

function callNativeApp () {
    try {
        webkit.messageHandlers.callbackHandler.postMessage("Hello from JavaScript");
    } catch(err) {
        console.log('The native context does not exist yet');
    }
}

            function onMapClick(e) {
//                popup
//                    .setLatLng(e.latlng)
//                    .setContent("You clicked the map at " + e.latlng.toString())
//                    .openOn(mymap);
                callNativeApp();
                
            }

            mymap.on('click', onMapClick);

