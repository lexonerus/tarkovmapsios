//
//  DatabaseHelper.swift
//  Tarkov Maps
//
//  Created by Alexey Krzywicki on 02/07/2019.
//  Copyright © 2019 lexone.ru. All rights reserved.
//

import SQLite

public class DatabaseHelper {
    
    var database: Connection!
    let sqlContract = SqliteContract()
    final let DATABASE_VERSION = 1;
 
    //MARK: Sqlite database creation
    func createDb() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory,
                                            in: .userDomainMask,
                                            appropriateFor: nil,
                                            create: true)
            
            let fileUrl = documentDirectory.appendingPathComponent("markers_storage").appendingPathExtension("db")
            let database = try Connection(fileUrl.path)
            self.database = database
            // Console log
            print("Database successfully created")
            
        } catch {
            print(error)
        }
    
    }
    
    func connectDb() {
        do {
            let path = NSSearchPathForDirectoriesInDomains(
                .documentDirectory, .userDomainMask, true
                ).first!
        
            let db = try Connection("\(path)/markers_storage.db")
            self.database = db
            print("Connection to DB was established.")
        } catch {
            print(error)
        }
        
    }
    
    //MARK: Sqlite tables creation in database
    func createTables() {
        let createTable1 = Table(sqlContract.TABLE_NAME_1).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable1)
            print("Table 1 was created")
        } catch {
            print(error)
        }
        
        let createTable2 = Table(sqlContract.TABLE_NAME_2).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable2)
            print("Table 2 was created")
        } catch {
            print(error)
        }
        
        let createTable3 = Table(sqlContract.TABLE_NAME_3).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable3)
            print("Table 3 was created")
        } catch {
            print(error)
        }
        
        let createTable4 = Table(sqlContract.TABLE_NAME_4).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable4)
            print("Table 4 was created")
        } catch {
            print(error)
        }
        
        let createTable5 = Table(sqlContract.TABLE_NAME_5).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable5)
            print("Table 5 was created")
        } catch {
            print(error)
        }
        
        let createTable6 = Table(sqlContract.TABLE_NAME_6).create { (table) in
            table.column(sqlContract.COLUMN_ID, primaryKey: true)
            table.column(sqlContract.COLUMN_UNIQID, unique: true)
            table.column(sqlContract.COLUMN_LAT)
            table.column(sqlContract.COLUMN_LNG)
            table.column(sqlContract.COLUMN_ICON, defaultValue: 0)
            table.column(sqlContract.COLUMN_ATTR, defaultValue: "no attr")
            table.column(sqlContract.COLUMN_POP, defaultValue: "no pop")
            table.column(sqlContract.COLUMN_FEATURE, defaultValue: "no feature")
        }
        
        do {
            try self.database.run(createTable6)
            print("Table 6 was created")
        } catch {
            print(error)
        }
    }
    
    func insertMarker() {
        connectDb()
        print("Starting insert procedure...")
        do {
            try self.database.run(Table(sqlContract.TABLE_NAME_1).insert(sqlContract.COLUMN_ID <- 0, sqlContract.COLUMN_LAT <- 0.234, sqlContract.COLUMN_LNG <- 0.333, sqlContract.COLUMN_UNIQID <- 777, sqlContract.COLUMN_ATTR <- "First Value"))
            print("Successfully inserted.")
        } catch {
            print(error)
        }
        
    }
    
    
    
    
}
