;
var markid = 0;
var oldmarkid;
var mapstate = 0;
var uniqid = 0;
var a = 0;
var datalength = Android.getDataLength();



      function showCoordinates (e) {
		popup
	    .setLatLng(e.latlng)
	    .setContent(MAP_CLICK + e.latlng.toString())
	    .openOn(mymap);
      }

      function centerMap (e) {
	      mymap.panTo(e.latlng);
      }

      function zoomIn (e) {
	      mymap.zoomIn();
      }

      function zoomOut (e) {
	      mymap.zoomOut();
      }

	  function setIcon(e) {
		    var id = usermarkers.getLayer(markid).options.uniqueId;
		    switch (a) {
  			    case 0:
    				this.setIcon(defaultmarker);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 1:
    				this.setIcon(weapon_box);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 2:
    				this.setIcon(computer);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 3:
    				this.setIcon(tools);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 4:
    				this.setIcon(keysp);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 5:
    				this.setIcon(safe);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 6:
    				this.setIcon(cabinet);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 7:
    				this.setIcon(loot2);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 8:
    				this.setIcon(jacket);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 9:
    				this.setIcon(penta);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 10:
    				this.setIcon(meds);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    case 11:
    				this.setIcon(weapon);
				    Android.setIcon(id, a);
				    a++;
    				break;
  			    default:
    				a = 0;
				    this.setIcon(defaultmarker);
				    Android.setIcon(id, a);
				    a++;
		    }
	  }

	function restoreIcon (e) {
		var id = usermarkers.getLayer(markid).options.uniqueId;
		var b = Android.restoreIcon(id);
		switch (b) {
  			case 0:
    				usermarkers.getLayer(markid).setIcon(defaultmarker);
    				break;
  			case 1:
    				usermarkers.getLayer(markid).setIcon(weapon_box);
    				break;
  			case 2:
    				usermarkers.getLayer(markid).setIcon(computer);
    				break;
  			case 3:
    				usermarkers.getLayer(markid).setIcon(tools);
    				break;
  			case 4:
    				usermarkers.getLayer(markid).setIcon(keysp);
    				break;
  			case 5:
    				usermarkers.getLayer(markid).setIcon(safe);
    				break;
  			case 6:
    				usermarkers.getLayer(markid).setIcon(cabinet);
    				break;
  			case 7:
    				usermarkers.getLayer(markid).setIcon(loot2);
    				break;
  			case 8:
    				usermarkers.getLayer(markid).setIcon(jacket);
    				break;
  			case 9:
    				usermarkers.getLayer(markid).setIcon(penta);
    				break;
  			case 10:
    				usermarkers.getLayer(markid).setIcon(meds);
    				break;
  			case 11:
    				usermarkers.getLayer(markid).setIcon(weapon);
    				break;
  			default:
				usermarkers.getLayer(markid).setIcon(defaultmarker);
		}
	}

	function markerInfo (e) {
		//Android.showToast(markid);
		Android.showToast(usermarkers.getLayer(markid).options.uniqueId);

      }
	function showAndroidToast (toast) {
        	Android.showToast(TEST_MESSAGE);
      }

	function getMarkerId(e) {
		    markid = usermarkers.getLayerId(this);
      }

    function getUniqidByMarkid(e) {
        uniqid = Android.getUniqidByMarkid(markid);
    }


	function insertMarkerToDb (e) {
		u3 = usermarkers.getLayer(markid);
		var response = JSON.stringify(u3.toGeoJSON().geometry.coordinates[0]);
		Android.showToast(CREATED_MARKER);

		Android.insertMarker(u3.toGeoJSON().geometry.coordinates[0],
			u3.toGeoJSON().geometry.coordinates[1], markid, usermarkers.getLayer(markid).options.uniqueId);
		//Android.showToast("Marker was successfully created");
	}

	function updateLatLng (e) {
	    var u6 = usermarkers.getLayer(markid);
	    var id6 = u6.options.uniqueId;
	    Android.updateLatLng(u6.toGeoJSON().geometry.coordinates[0], u6.toGeoJSON().geometry.coordinates[1], id6);
	}

	function deleteMarkerFromDb (e) {
		u4 = usermarkers.getLayer(markid);
		usermarkers.removeLayer(markid);
		Android.deleteMarker(u4.options.uniqueId);
		Android.showToast(DELETED_MARKER);
		datalength = Android.getDataLength();
	}
	function readMarkersFromDb (e) {
		datalength = Android.getDataLength();

		for (var i = 0; i < datalength; i++) {
			lat = Android.getLat();
			lng = Android.getLng();
			new L.marker([lat, lng]).addTo(mymap);
		}
	}

	function restoreUserMarkers () {
        if (usermarkers.getLayers().length == 0) {
  		//  block of code to be executed if the condition is true
		datalength = Android.getDataLength();
		Android.showToast(LOADED_MARKER);
		var lat = 0.0;
		var lng = 0.0;

		for (var i = 0; i < datalength; i++) {
			lat = Android.getLat(i);
			lng = Android.getLng(i);
			uniqid = Android.getUniqid(i);
			new L.marker([lng, lat], {
				uniqueId: uniqid,
				//icon: loot2,
				draggable:true,
				contextmenu: true,
    				contextmenuWidth: 140,
          			contextmenuItems: [
          			{
                                  text: CHANGE_ICON,
                                  index: 0,
                    	          callback: function (e) {
                    	            u5 = usermarkers.getLayer(markid);
                    	            var id5 = u5.options.uniqueId;
                    	            if (oldmarkid != markid) {
                    	                a = 1;
                    	                u5.setIcon(weapon_box);
                                        Android.setIcon(id5, a);
                                        a++;
                    	                oldmarkid = markid;
                    	            } else {
                    	                switch (a) {
                                      			case 1:
                                        				u5.setIcon(weapon_box);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 2:
                                        				u5.setIcon(computer);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 3:
                                        				u5.setIcon(tools);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 4:
                                        				u5.setIcon(keysp);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 5:
                                        				u5.setIcon(safe);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 6:
                                        				u5.setIcon(cabinet);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 7:
                                        				u5.setIcon(loot2);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 8:
                                        				u5.setIcon(jacket);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 9:
                                        				u5.setIcon(penta);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 10:
                                        				u5.setIcon(meds);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			case 11:
                                        				u5.setIcon(weapon);
                                        				Android.setIcon(id5, a);
                                        				a++;
                                        				break;
                                      			default:
                                    				    a = 0;
                                                        u5.setIcon(defaultmarker);
                                                        Android.setIcon(id, a);
                                                        a++;
                                    		}
                                    	}
                    	          }
                    },
          			{
              				text: DELETE_MARKER,
              				index: 1,
	      				callback: deleteMarkerFromDb
          			},
	  			    {
              				separator: true,
              				index: 2
          			}]
			})
			//.on('click', setIcon)
			//.on('dblclick', markerOnDblclick)
			.on('mouseover', getMarkerId)
			.on('add', getMarkerId)
			.on('moveend', function(e) {
			    markid = usermarkers.getLayerId(this);
			    u8 = usermarkers.getLayer(markid);
                var id8 = u8.options.uniqueId;
                Android.updateLatLng(u8.toGeoJSON().geometry.coordinates[0], u8.toGeoJSON().geometry.coordinates[1], id8);
			})
			.on('add', function(e) {
				//Android.showToast("Restore begin");
				var result = restoreIcon();
			})
			.addTo(usermarkers);
		}
           }
	}

var mymap = L.map('mymap', {
    	center: [0.01032, 0.0159],
    	zoom: 16,
   	layers: [base],
                  	contextmenu: true,
                      	contextmenuWidth: 140,
                  	contextmenuItems: [
                  	/*{
                  	    text: 'Show coordinates',
                  	    callback: showCoordinates
                  	},*/
                  	{
                  	    text: CREATE_MARKER,
                  	    callback: createMarker
                  	},  {
                  	    text: CENTER_MAP,
                  	    callback: centerMap
                  	}, '-', {
                  	    text: ZOOM_IN,
                  	    icon: 'images/zoom-in.png',
                  	    callback: zoomIn
                  	}, {
                  	    text: ZOOM_OUT,
                  	    icon: 'images/zoom-out.png',
                  	    callback: zoomOut
                  	}]
}).setView([0.01032, 0.0159], 16);




var usermarkers = L.layerGroup([]);

function createMarker (e) {

	if (mapstate == 0) {
		var result = restoreUserMarkers();
	}
	mapstate = 1;
	uniqid = Android.getUniqidLast();
	new L.marker(e.latlng, {
	uniqueId: uniqid,
	draggable:true,
	contextmenu: true,
    	contextmenuWidth: 140,
          contextmenuItems: [
	      {
              text: CHANGE_ICON,
              index: 0,
	          callback: function (e) {
	            u5 = usermarkers.getLayer(markid);
	            var id5 = u5.options.uniqueId;
	            if (oldmarkid != markid) {
	                a = 1;
	                u5.setIcon(weapon_box);
                    Android.setIcon(id5, a);
                    a++;
	                oldmarkid = markid;
	            } else {
	                switch (a) {
                  			case 1:
                    				u5.setIcon(weapon_box);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 2:
                    				u5.setIcon(computer);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 3:
                    				u5.setIcon(tools);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 4:
                    				u5.setIcon(keysp);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 5:
                    				u5.setIcon(safe);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 6:
                    				u5.setIcon(cabinet);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 7:
                    				u5.setIcon(loot2);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 8:
                    				u5.setIcon(jacket);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 9:
                    				u5.setIcon(penta);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 10:
                    				u5.setIcon(meds);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			case 11:
                    				u5.setIcon(weapon);
                    				Android.setIcon(id5, a);
                    				a++;
                    				break;
                  			default:
                				    a = 0;
                                    u5.setIcon(defaultmarker);
                                    Android.setIcon(id, a);
                                    a++;
                		}
                	}
	          }
          },/*
	      {
		      text: 'Get Info',
		      index: 2,
		      callback: markerInfo
	      },*/
	      {
             text: DELETE_MARKER,
             index: 1,
          	 callback: deleteMarkerFromDb
          },
	      {
              separator: true,
              index: 2
          }]
	})
	//.on('click', setIcon)
	//.on('dblclick', markerOnDblclick)
	.on('mouseover', getMarkerId)
	.on('add', getMarkerId)
	.on('add', insertMarkerToDb)
	.on('moveend', function(e) {
	    markid = usermarkers.getLayerId(this);
		u8 = usermarkers.getLayer(markid);
        var id8 = u8.options.uniqueId;
        Android.updateLatLng(u8.toGeoJSON().geometry.coordinates[0], u8.toGeoJSON().geometry.coordinates[1], id8);
	})
	.addTo(usermarkers);
	usermarkers.addTo(mymap);

}


new L.easyButton('<img src="images/load.svg" style="padding-top:20%; height: 20px;">', function () {
	if (mapstate == 0) {
		var result = restoreUserMarkers();
	} else {
	    Android.showToast(ALREADY_LOADED);
	}
	mapstate = 1;
	usermarkers.addTo(mymap);
}).addTo(mymap);

//Тестовая функция отображения координат

            var popup = L.popup();

            function onMapClick(e) {
                /*popup
                    .setLatLng(e.latlng)
                    .setContent(MAP_CLICK + e.latlng.toString())
                    .openOn(mymap);*/
                    ;
            }


            function onMapDblclick(e) {
	            ;
            }

mymap.on('click', onMapClick);
mymap.on('dblclick', onMapDblclick);