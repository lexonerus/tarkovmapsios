//
//  SqlContract.swift
//  Tarkov Maps
//
//  Created by Alexey Krzywicki on 02/07/2019.
//  Copyright © 2019 lexone.ru. All rights reserved.
//

import SQLite
//MARK: Database and Tables description
public final class SqliteContract {
    
    // Имена таблиц
    public final let TABLE_NAME_1 = "lab"
    public final let TABLE_NAME_2 = "woods"
    public final let TABLE_NAME_3 = "customs"
    public final let TABLE_NAME_4 = "interchange"
    public final let TABLE_NAME_5 = "factory"
    public final let TABLE_NAME_6 = "shoreline"
    
    // Имена столбцов
    public final let COLUMN_ID = Expression<Int64>("id")
    public final let COLUMN_UNIQID = Expression<Int64>("uniqid")
    public final let COLUMN_LAT = Expression<Double>("lat")
    public final let COLUMN_LNG = Expression<Double>("lng")
    public final let COLUMN_ICON = Expression<Int64>("icon")
    public final let COLUMN_ATTR = Expression<String>("attr")
    public final let COLUMN_POP = Expression<String>("poppup")
    public final let COLUMN_FEATURE = Expression<String>("feature")
    
}
