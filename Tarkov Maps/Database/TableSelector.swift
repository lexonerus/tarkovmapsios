//
//  TableSelector.swift
//  Tarkov Maps
//
//  Created by Alexey Krzywicki on 03/07/2019.
//  Copyright © 2019 lexone.ru. All rights reserved.
//

public class TableSelector {
    var sqlContract = SqliteContract()

    func selectTable(btnNumber: Int) -> String {
        switch (btnNumber) {
            case 1:
                    global.TABLE = sqlContract.TABLE_NAME_1
            case 2:
                    global.TABLE = sqlContract.TABLE_NAME_2
            case 3:
                    global.TABLE = sqlContract.TABLE_NAME_3
            case 4:
                    global.TABLE = sqlContract.TABLE_NAME_4
            case 5:
                    global.TABLE = sqlContract.TABLE_NAME_5
            case 6:
                    global.TABLE = sqlContract.TABLE_NAME_6
        default:
            print("works")
        }
        print(global.TABLE + " from selector")
        return global.TABLE
    }
}
