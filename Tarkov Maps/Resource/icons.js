// SETUP иконка выхода ЧВК
var exitpmc = L.icon({
    iconUrl: 'images/exit.png',

    iconSize:     [39, 39], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ДИКОГО
var exitscav = L.icon({
    iconUrl: 'images/exit-scav.png',

    iconSize:     [39, 39], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ЧВК возможно
var exitmb = L.icon({
    iconUrl: 'images/exit-maybe.png',

    iconSize:     [39, 39], // size of the icon
    iconAnchor:   [45, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var weapon_box = L.icon({
    iconUrl: 'images/weapon_box.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var weapon = L.icon({
    iconUrl: 'images/weapon.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var key = L.icon({
    iconUrl: 'images/key.png',

    iconSize:     [30, 30], // size of the icon
    iconAnchor:   [10,10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var tools = L.icon({
    iconUrl: 'images/toolbox.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var keysp = L.icon({
    iconUrl: 'images/keyspawn.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var safe = L.icon({
    iconUrl: 'images/safe.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var cabinet = L.icon({
    iconUrl: 'images/filecabinet.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var dead = L.icon({
    iconUrl: 'images/deadbody.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var spray = L.icon({
    iconUrl: 'images/sprayer.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var tent = L.icon({
    iconUrl: 'images/tent.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var penta = L.icon({
    iconUrl: 'images/pentagram.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var bag = L.icon({
    iconUrl: 'images/bag.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var meds = L.icon({
    iconUrl: 'images/medicine.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var loot2 = L.icon({	
	iconUrl: 'images/loot.png',
	iconSize:     [25, 25], // size of the icon});
});
var defaultmarker = L.icon({	
	iconUrl: 'images/marker-icon.png',
	iconSize:     [25, 41], // size of the icon});
});

var loot = L.icon({
    iconUrl: 'images/loot.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var armor = L.icon({
    iconUrl: 'images/armor.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var cash = L.icon({
    iconUrl: 'images/cash.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var computer = L.icon({
    iconUrl: 'images/computer.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var jacket = L.icon({
    iconUrl: 'images/jacket.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
var btn = L.icon({
    iconUrl: 'images/button.png',

    iconSize:     [25, 25], // size of the icon
    iconAnchor:   [30, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
