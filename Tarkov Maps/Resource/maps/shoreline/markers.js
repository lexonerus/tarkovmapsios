// SETUP иконка выхода ЧВК
var exitpmc = L.icon({
    iconUrl: 'images/exit.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ДИКОГО
var exitscav = L.icon({
    iconUrl: 'images/exit-scav.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 15], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// SETUP иконка выхода ЧВК возможно
var exitmb = L.icon({
    iconUrl: 'images/exit-maybe.png',

    iconSize:     [35, 35], // size of the icon
    iconAnchor:   [45, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});


//Слои для выходов ЧВК
var 	road = L.marker([0.00622, 0.00463], {icon: exitpmc}),
	roadp = new L.Marker([0.00622, 0.00463], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дорога на таможню|Customs road</div>'
    		})
	}),
    	lod = L.marker([0.01395, 0.01177], {icon: exitmb}),
	lodp = new L.Marker([0.01395, 0.01177], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка на причале|Boat</div>'
    		})
	}),
	kpp  = L.marker([0.01283, 0.0046], {icon: exitmb}),
	kppp = new L.Marker([0.01283, 0.0046], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Военный КПП|Military Checkpoint</div>'
    		})
	}),
	skal  = L.marker([0.00115, 0.0138], {icon: exitmb}),
	skalp = new L.Marker([0.00115, 0.0138], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход через скалы|Mountain trip</div>'
    		})
	}),
    	lod2 = L.marker([0.01395, 0.01177], {icon: exitmb}),
	lodp2 = new L.Marker([0.01395, 0.01177], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка на причале|Boat</div>'
    		})
	}),
	skal2  = L.marker([0.00115, 0.0138], {icon: exitmb}),
	skalp2 = new L.Marker([0.00115, 0.0138], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход через скалы|Mountain trip</div>'
    		})
	}),
	tun = L.marker([0.01051, 0.0219], {icon: exitpmc}),
	tunp = new L.Marker([0.01051, 0.0219], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Туннель|Tunnel</div>'
    		})
	});
var locationspmc1 = [
                ["LOCATION_1",0.01064, 0.02173],
                ["LOCATION_2",0.01007, 0.02259],
                ["LOCATION_3",0.00823, 0.02249],
                ["LOCATION_4",0.00746, 0.02222],
                ["LOCATION_5",0.00859, 0.02032],
		["LOCATION_6",0.00767, 0.01975],
		["LOCATION_7",0.00473, 0.02229],
		["LOCATION_8",0.005, 0.01968],
		["LOCATION_9",0.00293, 0.02036],
                ];

var locationspmc2 = [
                ["LOCATION_1",0.00109, 0.00946],
                ["LOCATION_2",0.00453, 0.00878],
                ["LOCATION_3",0.00597, 0.00644],
                ["LOCATION_4",0.00639, 0.00488],
                ["LOCATION_5",0.00777, 0.00694],
		["LOCATION_6",0.00902, 0.00337],
		["LOCATION_7",0.01057, 0.00568],
		["LOCATION_8",0.01112, 0.00754],
		["LOCATION_9",0.01274, 0.00541],
		["LOCATION_10",0.01312, 0.00687],
                ];

var pmc1 = L.layerGroup([road, roadp, lod, lodp, kpp, kppp, skal, skalp]);
var pmc2 = L.layerGroup([tun, tunp, lod2, lodp2, skal2, skalp2]);


for (var i = 0; i < locationspmc1.length; i++) {
                        circles = new L.circle([locationspmc1[i][1],locationspmc1[i][2]], 80, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")		
			.addTo(pmc1);

};
	
for (var i = 0; i < locationspmc2.length; i++) {
                        circles = new L.circle([locationspmc2[i][1],locationspmc2[i][2]], 80, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")		
			.addTo(pmc2);

};


//Слови для выходов Диких

var 	s1 = L.marker([0.0062, 0.00452], {icon: exitscav}),
	s1p = new L.Marker([0.0062, 0.00452], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дорога на таможню|Customs road</div>'
    		})
	}),
	s2 = L.marker([0.01418, 0.01019], {icon: exitscav}),
	s2p = new L.Marker([0.01418, 0.01019], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Маяк|Lighthouse</div>'
    		})
	}),
	s3 = L.marker([0.00469, 0.01223], {icon: exitscav}),
	s3p = new L.Marker([0.00469, 0.01223], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Вход в спортзал правое крыло|GYM</div>'
    		})
	}),
	s4 = L.marker([0.0039, 0.01304], {icon: exitscav}),
	s4p = new L.Marker([0.0039, 0.01304], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Подвал в админ. корпусе|Admin basement</div>'
    		})
	}),
	s5 = L.marker([0.00058, 0.00944], {icon: exitscav}),
	s5p = new L.Marker([0.00058, 0.00944], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход в заборе на ЮГЕ|South wall trip</div>'
    		})
	}),
	s6 = L.marker([0.0052, 0.02323], {icon: exitscav}),
	s6p = new L.Marker([0.0052, 0.02323], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Забор у разрушенного дома|Old house wall</div>'
    		})
	}),
	s7 = L.marker([0.00783, 0.02291], {icon: exitscav}),
	s7p = new L.Marker([0.00783, 0.02291], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Тупик Светлый|Light deadlock</div>'
    		})
	}),
	s8 = L.marker([0.01083, 0.02158], {icon: exitscav}),
	s8p = new L.Marker([0.01083, 0.02158], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Разрушенная дорога|Destroyed road</div>'
    		})
	});

var locationscavs = [
                ["LOCATION_1",0.01233, 0.00621],
                ["LOCATION_2",0.00717, 0.00558],
                ["LOCATION_3",0.00987, 0.00382],
                ["LOCATION_4",0.01212, 0.01337],
                ["LOCATION_5",0.00966, 0.00968],
		["LOCATION_6",0.00875, 0.01363],
		["LOCATION_8",0.00474, 0.0135],
		["LOCATION_9",0.00215, 0.01511],
		["LOCATION_10",0.00363, 0.01863],
		["LOCATION_11",0.00783, 0.01935],
		["LOCATION_12",0.01045, 0.02126],
		["LOCATION_13",0.01217, 0.01959],
		["LOCATION_14",0.007, 0.02249],
                ];

var scav = L.layerGroup([s1,s1p,s2,s2p,s3,s3p,s4,s4p,s5,s5p,s6,s6p,s7,s7p,s8,s8p]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 150, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")		
			.addTo(scav);
};

var clearmap = L.layerGroup([]);


//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});
//Добавление слоев на карту
var overlays = {
	"Чистая карта": clearmap,
	"ЧВК респ слева": pmc2,
	"ЧВК респ справа": pmc1,
	"Дикие" : scav
};
//Создание кнопки управлния слоями
L.control.layers(overlays).addTo(mymap);
