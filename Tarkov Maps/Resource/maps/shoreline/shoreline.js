var mymap = L.map('mymap').setView([0.01032, 0.0159], 16);

            L.tileLayer('maps/shoreline/{z}/{x}/{y}.png', {
            minZoom: 14,
            maxZoom: 18

            }).addTo(mymap);
            
            var popup = L.popup();

            function onMapClick(e) {
                popup
                    .setLatLng(e.latlng)
                    .setContent("You clicked the map at " + e.latlng.toString())
                    .openOn(mymap);
            }

            mymap.on('click', onMapClick);

