//
//  GlobalVariables.swift
//  Tarkov Maps
//
//  Created by Alexey Krzywicki on 03/07/2019.
//  Copyright © 2019 lexone.ru. All rights reserved.
//

struct global {
    static var x = 0
    static var btnNumber = 0
    static var mapstate = 0
    static var TABLE = ""
    static var langid = 1 // 0 - RU, 1 - EN
}

