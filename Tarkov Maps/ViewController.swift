//
//  ViewController.swift
//  Tarkov Maps
//
//  Created by lexone on 27/04/2019.
//  Copyright © 2019 lexone.ru. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler {
    
    @IBOutlet var myWebKit: WKWebView!
    
    override func viewDidLoad() {
        
        let userContentController = WKUserContentController()
        let config = WKWebViewConfiguration()
        let mapstate = global.mapstate
        let preferences = WKPreferences()

        
        super.viewDidLoad()
        
        // Enable JavaScript support. It's important line!
        preferences.javaScriptEnabled = true
        
        //MARK: WebView setup
        config.preferences = preferences
        config.userContentController = userContentController
        self.myWebKit = WKWebView(frame: self.view.bounds, configuration: config)
        myWebKit = WKWebView(frame: view.bounds, configuration: config)
        myWebKit.isUserInteractionEnabled = true
        myWebKit.scrollView.isScrollEnabled = false
        view.addSubview(myWebKit)
        
        myWebKit.configuration.userContentController.add(self, name: "getLangId")
        myWebKit.configuration.userContentController.add(self, name: "jsHandler")
        myWebKit.configuration.userContentController.add(self, name: "insertMarker")
        
        //MARK: Adding webView content
        switch mapstate {
        case 1:
            loadMap(mapname: "index")
            print("index.html")
        case 2:
            loadMap(mapname: "woods")
            print("load woods.html")
        case 3:
            loadMap(mapname: "customs")
            print("load customs.html")
        case 4:
            loadMap(mapname: "interchange")
            print("load interchange.html")
        case 5:
            loadMap(mapname: "factory")
            print("load factory.html")
        case 6:
            loadMap(mapname: "shoreline")
            print("load shoreline.html")
            
        default:
            print(global.mapstate)
            print("default")
        }
        
    }
    
    func loadMap(mapname: String) {
        do {
            guard let filePath = Bundle.main.path(forResource: mapname, ofType: "html")
                else {
                    // File Error
                    print ("File reading error. No \"\(mapname).html\" were founded.")
                    return
            }
            
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            myWebKit.loadHTMLString(contents as String, baseURL: baseUrl)
            
        }
        catch {
            print ("File HTML error")
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let dbHelper = DatabaseHelper()
        var jsVar = 0
        
        switch message.name {
        case "getLangId":
            print("langid = \(global.langid)")
            myWebKit.evaluateJavaScript("var langid = \(global.langid);", completionHandler: nil)
        case "jsHandler":
            jsVar = message.body as! Int
            myWebKit.evaluateJavaScript("changeText();", completionHandler: nil)
            print(jsVar + 10)
        case "insertMarker":
            print("insert")
            dbHelper.insertMarker()
        default:
            print("error")
        }

    }
}
